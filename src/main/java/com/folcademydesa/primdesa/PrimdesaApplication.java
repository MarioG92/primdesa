package com.folcademydesa.primdesa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimdesaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimdesaApplication.class, args);
	}

}
