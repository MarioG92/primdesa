package com.folcademydesa.primdesa.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodbyeController {

    @RequestMapping("/goodbye")
    public String goodbye(){
        return "Goodbye world";
    }
}
